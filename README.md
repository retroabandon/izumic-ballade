Izumic Ballade
==============

Reverse-engineering and translation of the game _Izumic Ballade_.

There are at least two versions of this game available; see
[`doc/version.md`](doc/version.md) for more details. The game also saves to
its distribution diskette, so copies from different sources may have
different (or no) save files on them.


Files and Organization
----------------------

While the MSX file system is case-insensitive, our convention is to use
lower-case for all filenames in this repo because that is openMSX's default
when creating new files in a host filesystem.

- `bin/`: Programs used to help with reverse-engineering.
- `doc/`: Detailed documention on various aspects of the system.
- `disk/`: Working directory for the emulator's diskettte; no files are
  committed under this.
- `files/`: All the files from the 1997 msx.org release. This may include
  save data that did not come with the original game.
- `info/`: Information related to reverse-engineering, such as symbol
  definitions for the disassembler.
- `src/*.ba`: The code from `files/*.bas` saved in detokenized form. See
  "Source Code" below for more information.
- `tmp/`: For developer working files; ignored by Git.
- `tool/bastok/`: Detokenization tool used to create/read files under `src/`.

### Documentation

From the `doc/` subdirectory here:
- [Emulation](doc/emulation.md): How to set up and run an MSX emulator to
  work on the code here.
- [Theory of Operation](doc/optheory.md): An (incomplete) description of
  the game's program design.
- [Game Character Sets and Fonts](doc/charset.md)
- [Game Versions](doc/versions.md): Sources and versions of the game.

Elsewhere:
- [`sedoc/msx`][se-msx]: cjs's general technical notes on MSX systems.
  Particularly applicable is the [MSX BASIC Technical
  Information][se-basic] document.


Submodules
----------

This repo uses [`bastok`][] (MSX BASIC tokenization tools) as a submodule
at `tool/bastok/`. If not cloned with the `--recursive` option, do a `git
submodule update --init` to check out the submodule. It is also a good idea
to run the tests (`tool/bastok/Test`) after the checkout.


Branches
--------

- `master`: Main work branch
- `2020-08-16`: A previous main work branch that has been abandoned. It
  contains a non-working partial English translation.
- `dev/DEVNAME/DATE/TAG`: Development branches used by developer _devname_,
  with an optional _date_ code when it was started (to make it easier to
  remove old work) and _tag_ describing what work is on the branch. All
  development branches should follow this format.


Source Code
-----------

### BASIC

The `src/*.ba` files are "source code" generated from the tokenized
`files/*.bas` files. They've been detokenized with `tool/bastok/` and are
UTF-8 Unix-format text files, with LF line endings and no ^Z at the end.
The BASIC program code itself is all printable ASCII characters (which are
the same in ASCII and UTF-8 encodings); the text in string constants
(between double quotes) and DATA statement arguments is a UTF-8 conversion
from the MSX Japanese character set, using the internal conversion table
from `bastok`. This is not correct for many of the files, which use a
custom character set; that will be dealt with later when `bastok` is
extended to support custom MSX charset/UTF-8 conversion. Also see [Game
Character Sets and Fonts](doc/charset.md) for further details.

We use the Unicode U+2016 `‖` "double vertical line" character to mark the
start of source code comments which terminate at the end of the line (file
line, not BASIC line). This was chosen because it's not easily confused
with any of the characters from the standard MSX character sets. The
standard RFC 1345 digraph for this is `!2`, and you can enter it in Vim
using Ctrl-K followed by that digraph. Note that this is not the same as
U+2551 `║` "box drawings double vertical."

These files are named with extension `.ba` instead of `.asc` for two good
reasons: 1) they are not the same as the output of `SAVE "filename.asc",A`
and are not readable by the `LOAD` command, and 2) `.asc` is used on modern
systems for other purposes, such as GnuPG ASCII-armored format.

### Z80 Assembly

> WARNING: No attempt has yet been made to re-assemble the disassemblies of
> the machine-code files. Currently the game is always run using the
> original machine-code files.

The `src/*.asm` files are annotated disassemblies of machine-code files
from `files/`. They were originally disassembled using [z80dasm]; the
`.sym` and `.block` information files used for these disassemblies are
under `info/`.

To regenerate or generate new disassemblies use the `bin/disassemble`
script, which requires Python 3.5 or above. It can be run directly on Unix,
or via `py bin/disassemble` on Windows. It also requires `z80dasm` to be
installed and available in the path; on Debian/Ubuntu systems install with
`apt-get install z80dasm`; for Windows see the [z80dasm home
page][z80dasm].

`bin/disassemble` should be given a path to a machine-code file in MSX
BASIC format (type 0xFE). After removing the file's header it will run the
disassembler on the machine code using any corresponding `.sym` and
`.block` files under `info/` and print the result to stdout. The
documentation in `bin/disassemble` and the `z80dasm` manual page provide
further information.



<!-------------------------------------------------------------------->
[`bastok`]: https://github.com/0cjs/bastok
[se-basic]: https://github.com/0cjs/sedoc/blob/master/8bit/msx/basic.md
[se-msx]: https://github.com/0cjs/sedoc/tree/master/8bit/msx
[z80dasm]: https://www.tablix.org/~avian/blog/articles/z80dasm/
