;   i.mch: Search for and init (if present) an MSX-MUSIC extension
;
;   This is used `autoexec.ba`, and returns the slot descriptor of the
;   MSX-MUSIC extension in location `maxsafe`, or 0 if MSX-MUSIC was not
;   found.
;
;   THIS INCLUDES SELF-MODFING CODE. Modified instructions are have a label
;   ending in `PARAM` and are marked `MOD` in the comments.
;

; ----------------------------------------------------------------------
;   MSX System Routines and Addresses

;   ♣AF,BC,DE Read byte from slot and return it in A
;      A: x000eepp x=1 if expanded slot, ee=expanded slot #, pp=primary slot #
;     HL: address to read ($0000-$BFFF)
;
RDSLT       equ $000c

;   ♣all Disables interrupts and banks in a given slot for a page.
;      A: x000eepp x=1 if expanded slot, ee=expanded slot #, pp=primary slot #
;     HL: any address in the target page
;
ENASLT      equ $0024

;   Base address of page 1. This is where the MSX-MUSIC ROM resides and is
;   also the page in RAM to which we copy the MSX-MUSIC ROM if we find it.
;
p1base      equ $4000           ; base address of page 1

;   Disk ROM stores the main RAM (detected at startup) slot IDs at $F341-$F344
;   for pages 0-3. https://www.msx.org/wiki/How_to_detect_the_RAM
;
DR_MRAM_P1  equ $F342

;   Table of expansion status of slots. For each slot, 0 to 3, its
;   corresponding byte contains $80 if expanded (i.e., has secondary
;   slots), $00 if it's primary-only.
;
EXPTBL      equ $fcc1           ; $80=expanded $00=not for slots 0, 1, 2, 3

;   MSX-MUSIC I/O port enable memory address.
;   Set b0=1 to enable, b0=0 to disable.
;   See: https://github.com/0cjs/sedoc/tree/master/8bit/msx#msx-music
;
MMIO_ENA    equ $7FF6

; ----------------------------------------------------------------------

;   Location used for return value to BASIC.
;
;   Though the BASIC code calling this CLEARed HIMEM to well below this
;   value, this isn't actually truly safe because it got wrong the test
;   that this location was below HIMEM _before_ the CLEAR. However, the
;   English MSX2 Technical Handbook suggests that using memory up to this
;   address should almost always be safe, as DOS even with a "2DD-2 drive"
;   will not go past this, which probably explains why it seems to work on
;   most systems.
;
retval      equ $DE3F

;   If we find it, we copy the first 3.5 KB of the MSX-MUSIC ROM to RAM,
;   presumably so that we can use the remaining RAM on that page for other
;   things.
;
datalen     equ $0E00

; ----------------------------------------------------------------------

            org $c800

usr:        ;   BASIC entry point from USR() call

            ;   Stage 1: MODIFY initmusic to search for
            ;   the internal MSX-MUSIC signature.
            ld a,LSB(sigIaddr)  ; address we in page 1 we will be checking:
            ld (sigadrPARAM+1),a;   setup load of HL (LSB)
            ld hl,sigI          ; the signature we're looking for:
            ld (sigPARAM+1),hl  ;   setup load of DE
            ld a,sigIlen        ; signature length:
            ld (siglenPARAM+1),a;   setup load of B
            xor a               ; 0 = internal search stage
            ld (extsearch),a

startslots: ld bc,EXPTBL-1
            ld d,0              ; initial primary slot number
chkslot:    inc c               ; next entry in slot expansion status table
            ld a,(bc)           ; load entry
            cp $80              ; is it expanded (bit 7 set)?
            jp nc,expanded      ; yes, check secondary slots
            call initmusic      ; check only primary slot
nextpri:    inc d               ; next primary slot
            ld a,d
            cp 4                ; have we reached last primary slot?
            jp nz,chkslot       ; no, continue

            ;   Stage 2: MODIFY initmusic to search for the external
            ;   MSX-MUSIC signature. (We actually do this again after we're
            ;   done because we don't check to see if we've already
            ;   completed this stage until the end of this block.)
            ld a,LSB(sigEaddr)
            ld (sigadrPARAM+1),a
            ld hl,sigE
            ld (sigPARAM+1),hl
            ld a,sigElen
            ld (siglenPARAM+1),a
            ld hl,extsearch
            ld a,(hl)           ; ld a,extsearch: get previous search stage
            inc (hl)            ; inc extsearch:  move to next search stage
            or a                ; were we already on external search stage?
            jp z,startslots     ; no, start that stage

            ;   Return 0 to BASIC indicating that we did not discover an
            ;   MSX-MUSIC expansion. The MSX specification states that the
            ;   MSX-BASIC interpreter will always be in slot 0 pages 1 and 2
            ;   (Sony MSX1 Technical Manual p.162), so it's safe to use
            ;   0 as a return value here indicating "no MSX-MUSIC."
            xor a               ; return 0
            ld (retval),a       ;    to BASIC
            ei                  ; enable interrupts disabled by ENASLT
            ret

expanded:   set 7,d             ; mark current slot as expanded
            call initmusic
            ld a,d
            add a,4             ; increment secondary slot number
            ld d,a
            and $10             ; secondary slot number overflow?
            jp z,expanded       ; no, continue checking
            ld a,d
            and 3               ; mask out all but primary slot number
            ld d,a
            jp nextpri          ; continue with next primary slot

;   Check for an MSX-MUSIC signature. If not found, return.  if found, set
;   the return value for BASIC, copy 3.5 KB of its ROM into system RAM, and
;   drop our caller's return address so that we return directly to BASIC. D
;   should contain the slot descriptor in standard ExxxSSPP form; other
;   parameters are set via modfiying the operands of the *PARAM
;   instructions below.
;
;   EXTERNALLY MODIFIED CODE. POSSIBLY NO RETURN.
;
initmusic:
sigadrPARAM:ld hl,sigEaddr      ; MOD LSB: address to check for signature
            ld a,d              ; slot descriptor
            ld (slotPARAM+1),a  ; setup ld a,slot_descriptor
            push de             ; save slot descriptor (D)
            push bc             ; save current entry in EXPTBL
sigPARAM:   ld de,sigI          ; MOD WORD: pointer to sig we're looking for
siglenPARAM:ld b,sigElen        ; MOD: length of signature we're checking
                                ;   (loop counter for djnz below, 4 or 8)
s2:         push de             ; save signature pointer
            push bc             ; save loop counter
slotPARAM:  ld a,0              ; MOD: load slot descriptor
            call RDSLT          ; load next byte of signature area into A
            pop bc              ; restore loop counter
            pop de              ; restore signature pointer
            ld c,a              ; byte from signature area
            ld a,(de)           ; byte at signature pointer
            inc e               ; increment signature pointer
            inc l               ; increment slot memory read address
            cp c                ; does sig addr byte match our search byte?
            jp nz,s4ret         ; not the byte we're looking for, return
            djnz s2             ; decrement remaining length to check in B
                                ;   and continue checking if not 0

            ;   We've completed a match: setup return.
            pop bc
            pop af              ; load slot descriptor parameter to A
            ld (retval),a       ; set that as our return value to BASIC

            ;   Copy 3.5 KB out of the slot we've selected for page 1 into
            ;   a temporary buffer that we will later copy to RAM in page 1.
            ld h,MSB(p1base)    ; page to bring in
            call ENASLT         ; bring in the slot we found
            ld hl,p1base        ; src addr
            ld de,tmpbuf        ; dest addr
            ld bc,datalen
            ldir                ; copy

            ld a,(extsearch)
            or a                ; are we on an internal ROM search?
            jr z,loadP1ram      ; yes, carry on with copy

            ld a,(MMIO_ENA)     ; not internal; we must enable the
            or 1                ;   MSX-MUSIC I/O ports
            ld (MMIO_ENA),a

            ;   Bring main RAM into page 1, copy the 3.5 KB from tmpbuf to
            ;   the start of it, and then restore page 1 to ROM from slot 0.
loadP1ram:  ld a,(DR_MRAM_P1)   ; main RAM page 1 slot descriptor
                                ; (set by Disk ROM at startup)
            call ENASLT         ; bring in RAM
            ld hl,tmpbuf        ; src addr
            ld de,p1base        ; dest addr
            ld bc,datalen
            ldir                ; copy
            ld a,(EXPTBL)       ; slot 0 or expanded slot 0.0 descriptor
            ld h,MSB(p1base)    ; page to bring in
            call ENASLT         ; bring back system ROM
            pop hl              ; remove return address of our caller so
                                ;   that we return directly to BASIC
            ret

s4ret:      pop bc
            pop de
            ret

;   The search is done in two stages: first for an internal MSX-MUSIC
;   signature (sigI) and then for an external MSX-MUSIC signature (sigE).
;   This location holds that state: 0 for internal, 1 for external.
;
extsearch:  defb 0

; ----------------------------------------------------------------------
;   Definitions for the signatures to detect MSX-MUSIC hardware.

sigIaddr    equ $4018           ; start address of internal ROM signature
sigEaddr    equ $401C           ; start address of external ROM signature

sigIlen     equ 8               ; length of sigI signature
sigElen     equ 4               ; length of sigE signature

sigI:       defm 'APRL'         ; internal MSX-MUSIC signature (includes sigE)
sigE:       demf 'OPLL'         ; external (cart) MSX-MUSIC signature

tmpbuf:                         ; $C8C9 (up to retval $DE3F, a bit over 5 KB)
