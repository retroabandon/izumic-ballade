MSX System Emulation
====================

This project uses [openMSX][omsx] ([Wikipedia][wp], [GitHub][gh])
for testing and tool support. One reason for this is that it can be
controlled by an external process.

The `run` script will run the emulator (`./run -h` for help; ensure you're
using "Git Bash" if on Windows). If you get errors from openMSX see the
setup and installation information below.

Documentation:
- [openMSX manuals][man]
- [openMSX Setup Guide][setup]
- [openMSX User's Manual][userman]
- [openMSX Console Command Reference][cmdref]
- [Diskmanipulator][dm]


Setup
-----

Installation:
- Debian and Ubuntu Linux:
  - Install: `sudo apt-get install openmsx`
  - ROM images go in `~/.openMSX/share/systemroms/`.
- Windows:
  - Install: download using links at left-hand side of [home page][omsx].
  - ROM images go in `C:\Users\<username>\Documents\openMSX\share\systemroms\`.

For copyright reasons, openMSX comes with the open-source C-BIOS ROM.
This does not include BASIC (and often has other compatibility issues)
and so can't be used to run this game. ROM images for other machines
can be downloaded from [`file-hunter.com`][srom].

For testing we use the following openMSX machines.
- [`Sony_HB-F1XD`][f1xd]: ROM images `hb-f1xd_*.rom`.
- [`Panasonic_FS-A1F`][fs-a1f]: ROM images `fs-a1f_*.rom` and
  `Panasonic_FS-A1F_DA1024D0365R.rom`. Slow startup, and starts at a menu,
  but this might be fixed with a switch setting.

Typical startup for a manually run machine, mounting the contents of
`disk/` as the internal drive, is: `openmsx -machine Sony_HB-F1XD -diska
disk/`. `F10` will bring up the console for configuration and management.
See User's Manual [2. Starting the Emulator][starting] for more details.

Neither of these include FM sound, so choose "PSG", not "FM" at startup if
prompted. (An FM-PAC cart can be added with [`ext fmpac`][ext]; this
requires `fmpac.rom` from the [extension ROMs][erom] and is untested.)

#### Selected Console Commands and Settings

The console has case-sensitive tab-completion and a `help` command.

    machine MACHNAME            # Set emulation to a particular machine.
    toggle fullspeedwhenloading # For faster loads from disk
    reset                       # reset computer
    diska DIR                   # Set disk directory; may also be .dsk image


Other Emulators
---------------

- [blueMSX][bl wp] Windows-only. Seems to be dying or dead; last release
  was 2009 and the `bluesmx.com` site is gone.
- [fMSX][fm wp]. Little known about this?
- Many others [listed in the MSX FAQ][msxnet].


<!-------------------------------------------------------------------->
[cmdref]: https://openmsx.org/manual/commands.html
[dm]: https://openmsx.org/manual/diskmanipulator.html
[gh]: https://github.com/openMSX/openMSX
[man]: http://openmsx.org/manual/
[omsx]: https://openmsx.org/
[setup]: https://openmsx.org/manual/setup.html
[userman]: https://openmsx.org/manual/user.html
[wp]: https://en.wikipedia.org/wiki/OpenMSX

[erom]: https://download.file-hunter.com/System%20ROMs/extensions/
[f1xd]: https://msx.org/wiki/Sony_HB-F1XD
[fs-a1f]: https://msx.org/wiki/Panasonic_FS-A1F
[srom]: https://download.file-hunter.com/System%20ROMs/machines/

[ext]: https://openmsx.org/manual/commands.html#ext
[filepool]: https://openmsx.org/manual/commands.html#filepool
[starting]: https://openmsx.org/manual/user.html#starting

[bl wp]: https://en.wikipedia.org/wiki/BlueMSX
[fm wp]: https://en.wikipedia.org/wiki/FMSX
[msxnet]: https://faq.msxnet.org/msxemu.html
