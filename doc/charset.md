Game Character Sets and Fonts
=============================

The only use of the standard MSX character set seems to be in `intro.bas`
lines 59, 66 and 67. Otherwise the intro and game both use custom fonts
with non-standard character code mappings.

These can be found in the `2020-08-16` branch:
- `izumic_bigfont`: used for the intro
- `izumic_smallfont`: used for most of the game.
