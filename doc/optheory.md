Theory of Operation
===================

The game is spread across 14 BASIC files that load each other, starting
with `autoexec.bas` which is loaded automatically when the system boots.

#### Program Files

- `autoexec.bas`: Initial startup
- `ato.bas`: "Atogaki" (trailer); preview of next game.

#### Run Graph

- `autoexec.bas`: auto-loaded by MSX on boot.
  - →`ato.bas`: started if enabled by location $FBE5=251 magic (possibly
    holding down a key)
  - →`init.bas`


Details
-------

The `RUN "filename.bas"` command will clear all BASIC variables; to
transfer data between the different programs/overlays it saves data to
`MEM:SVD.DAM` before `RUN`ning the next overlay.
- `MAIN.BAS` (similar code is in other files):
  - 710 saves: `GOSUB 710` before most `RUN` statements.
  - 730 reloads; the setup in line 10 calls it with `GOSUB 730`.
