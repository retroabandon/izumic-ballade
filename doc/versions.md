Game Versions
=============

The game versions that we know of are:
- 1996-04-01: Takeru retail diskette owned by Wyrdwad.
- 1997-06-25: [From msx.org][msxorg page] ([ZIP file][msxorg zip]).

The dates are from the file timestamps of `main.bas` on the disk/in the
disk image file. Timestamps on other files may be erronous; some of have
timestamps from 2000 (see below) and others have no timestamps at all.

The Git history of the `files` directory contains the 1996 Takeru retail
version of the game.


File Differences
----------------

- `c2.vr6`, `font.vr3`, `map7.vr4`, `md6.bin`: All have timestamps from
  2000 (and are the same) in the 1996 and 1997 versions.
- `autoexec.bas`: 1997 version changes:
  - 10: `POKE -1103,1` pokes `0` instead,
  - 10→20: `CLEAR300,&H9DA0` moved to after `ELSE` in line 20.
  - 260: New version adds `PRESET(0,0):` at start of line.
- `0.dam`, `ato.bas`, `bh.bas`, `bi.bas`, `bj.bas`, `cf.bas`, `ev.bas`,
  `ev2.bas`, `fd.vr5`, `hs0.dat`, `hs1.dat`, `i.mch`, `init.bas`,
  `main.bas`, `map1.vr4`, `mpsc.mch`, `open.bas`, `s`, `s.bas`, `s.z80`,
  `st0.dis`: Different between 1996 and 1997 versions.
- `1.dam`, `2.dam`, `5.dam`, `6.dam`, `font.pl5`: Only in 1997 version.



<!-------------------------------------------------------------------->
[msxorg page]: https://www.msx.org/downloads/games/role-playing-games-rpg/izumic-ballade
[msxorg zip]: https://www.msx.org/download/download/2012/06/izumicballade.zip
